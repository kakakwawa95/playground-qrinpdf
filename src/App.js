import React     from 'react'
// import QRcode    from 'qrcode.react'
// import { jsPDF } from 'jspdf'
// import {PDFtoIMG} from 'react-pdf-to-image';
// import PDF from "./example_hyundai.png"
// import file from "./RSV_2022_00025.pdf"
// import * as pdfjsLib from 'pdfjs-dist'
import QRCodeGenerator from './QRCodeGenerator';

// const PDFImg = () =>
//     <>
//         <PDFtoIMG file={file}>
//             {({pages}) => {
//                 if (!pages.length) return 'Loading...';
//                 return pages.map((page, index)=>
//                     <img key={index} src={page}/>
//                 );
//             }}
//         </PDFtoIMG>
//     </>
//  function onUpload(files) {
//     if (files.length !== 1) return;
//     const file = files[0];
//     let reader = new FileReader();
//     reader.onload = e => {
//       const data = atob(e.target.result.replace(/.*base64,/, ''));
//       renderPDF(data);    
//     }
    
//     reader.readAsDataURL(file);
//   }
   
  
//   async function renderPDF(data) {
//     const pdf = await pdfjsLib.getDocument({data}).promise;
    
//     for (let i = 1; i <= pdf.numPages; i++) {
//       const image = document.createElement('img');
//       document.body.appendChild(image);
      
//       const page = await pdf.getPage(i);
//       const viewport = page.getViewport({scale: 2});
//       const canvas = document.createElement('canvas');
//       const canvasContext = canvas.getContext('2d');
//       canvas.height = viewport.height;
//       canvas.width = viewport.width;
//       await page.render({canvasContext, viewport}).promise;
//       const data = canvas.toDataURL('image/png');
//       image.src = data;
//       image.style.width = '100%';
//     }
//   }
// const App = () => {

//     const generatePDF = () => {

//         // Defines the pdf
//         var pdf = new jsPDF({
//             orientation: 'portrait',
//             unit: 'mm',
//             format: 'a4'
//         })


//         let base64Image = document.getElementById('qrcode').toDataURL()

//         pdf.setProperties({
//             title: "Some Letters From Hyundai"
//         });

//         pdf.text("Hello world!", 100, 100);

//         // Adds the image to the pdf
//         pdf.addImage(PDF, "JPEG", 0, 0, 200, 200);
//         pdf.addImage(base64Image, 'png', 135, 135, 0, 0)
        
//         // Downloads the pdf
//         // pdf.save('QR.pdf')
//         window.open(URL.createObjectURL(pdf.output("blob")))
    
//     }
    

//     return (
//         <>
//             <QRcode value = {'https://erikmartinjordan.com'} id = 'qrcode' style={{display: 'none'}}/>
            
//             <PDFImg/>
//             <button onClick = {generatePDF}>Download pdf</button>
//         </>
//     )

// }


const App = () => {

    return (
        <div>
            <h1>Keluar QR</h1>
            <QRCodeGenerator/>
        </div>
    )

}

export default App