import React from "react";
import { Page, Text, View, Document, StyleSheet, Image } from '@react-pdf/renderer';
import logo from "./logo.svg"

const styles = StyleSheet.create({
    page: {
      backgroundColor:'#ffffff',
      display : 'block'
    },
    view: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'center',
      width: '100%',
    },
    QRImage: {
        width: '100%',
        height: '100%',
    },
    text: {
        color: '#0061C6',
        size: '11em',
    },
    logoImage: {
        width: '25%',
        height: '50%',
    }
  });
  

  // Create Document Component
  const PDFcomponent = props => {
    const { PDFImageIds } = props;
    const IdsArray = [];
    const resultArray = PDFImageIds.map(id => {
        IdsArray.push(id);
        return document.getElementById(id).toDataURL();
    });
    <Document>
     {resultArray.map((dataURL, id) => {
        return (
            <Page size="A4" key={`PageID_${id}`} style={styles.page}>
                <View style={styles.view}>
                    <Image allowDangerousPaths src={dataURL} style={styles.QRImage}/>
                </View>
                <View style={styles.view}>
                    <Text style={styles.text}>{IdsArray[id]}</Text>
                </View>
                <View style={styles.view}>
                    <Image src={logo} style={styles.logoImage}>{IdsArray[id]}</Image>
                </View>
            </Page>
            );
        })}
    </Document>
  };


  export default PDFcomponent